#!/bin/bash

# helper functions
RED='\033[1;31m'
BLUE='\033[1;34m'
GREEN='\033[1;32m'
NOCOLOR='\033[0m'

function colorecho() {
  echo -e "${BLUE}[MESSAGE] $@${NOCOLOR}"
}

# list of packages separated by spaces
PACKAGES="kitty imagemagick fzf bat exa httpie zoxide atool tmux fisher ranger starship streamlink mpv"
QEMU="qemu virt-manager dnsmasq iptables ebtables swtpm edk2-ovmf"

# Setup distrobox with packages listed above
colorecho "Setting up distrobox home container"
distrobox create --image archlinux --name home --additional-packages $PACKAGES

# update fish plugins - assumes you already have your fish config and everything placed
distrobox enter home -- fisher update

colorecho "Complete"
